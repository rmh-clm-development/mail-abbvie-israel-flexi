module.exports = {

	legacy: false,

  // folder for all images (There is no need to change this for a Veeva setup )

  imagesDirName: 'images',

  // JSON or YAML
  templateDataFile: 'data.json',

  // test mail setup

  mailSubject: 'RMH Testmail',

  defaultTestMail: 'rmh@litmusemail.com',

  renderVeevaToken: false,

  sc2RawTokenOutput: false,
  devRawTokenOutput: false,
  testRawTokenOutput: false,

  // optionial
  mailSubjectOfTemplate: {
    'material-4247': 'Promotional information as requested'
  },

  /* CDN URL for images */

  s3Bucket: 'aws-website-html-mail-assets-i3o3o',

  cdnUrl: {
    test: 'https://s3.amazonaws.com/aws-website-html-mail-assets-i3o3o/mail-abbvie-israel-flexi/',
    dist: 'https://s3.amazonaws.com/aws-website-html-mail-assets-i3o3o/mail-abbvie-israel-flexi/'
  },

  showcase2: {
    /**
         * Endpoint to connect with a specific SC2 instance
         */
    url: 'http://abbvie.api.showcase2.staging3.lan.rmh.de/api/v1',

    /**
         * Contains default options for specific actions
         * e.g. upload for uploading content, download for downloading content etc.
         */
    defaults: {

      /**
             * Default options for uploading content to SC2
             */
      upload: {

        /**
                 * Update the material model with the following options once
                 * the date gets pushed to the SC2 instance
                 */
        update: {},

        /**
                 * Viewport setup – width & height can be used to specify viewport dimensions
                 * for image renditions and live view
                 */
        viewport: {
          width: 620
        }
      }
    }
  }

}
