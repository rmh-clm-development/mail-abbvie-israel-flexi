# Introduction

### upload to a NOT protected open webpage for test mailings

_copies the contents of /images to the Amazon AWS:_

    grunt --project=mail-abbvie-israel-flexi --template=material-xxxx s3-upload

### Development

file will be rendered to the /build folder

    grunt --project=mail-abbvie-israel-flexi --template=material-xxxx

### Distribution

file will be rendered to the /dist folder

    grunt --project=mail-abbvie-israel-flexi --template=material-xxxx dist

### Test

the grunt task **test** sends an email to the email address defined in **defaultTestMail**. Best practice is to send mails to litmus.com
See vars in config.cfg.js

    grunt --project=mail-abbvie-israel-flexi --template=material-xxxx test

override the default email-address to send mails to another email address

    grunt --project=mail-abbvie-israel-flexi --template=material-xxxx test --mailto=john.doe@email.com

## Useful Adobe-Campaign token

Salutation

    Dear <%= recipient.salutation %> <%= recipient.firstName %> <%= recipient.lastName %>

Veeva-ID / Account ID

    <%= recipient.uniqueAccountId %>

Mirror Page

    <%@ include view=\'MirrorPageUrl\' %>

Unsubscribe Link

    <%= escapeUrl(cryptString(recipient.id)) %>
    https://t.information.boehringer-ingelheim.com/webApp/bipGbPreferences?id=<%= escapeUrl(cryptString(recipient.id)) %>
