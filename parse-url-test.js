const URL = require('url').URL
const theParser = require('url')

module.exports = {

  parser: function (href, projectPath) {

    // Parser to avoid invalid URL. This function repairs some URL
    href = theParser.parse(href).href

    var _fields = require(projectPath + '/showcase/fields.json')
    const _services = require(projectPath + '/showcase/services.json')
    var boilerplate = _services.fragger.boilerplate

    href = href.replace(/&amp;/g, '&')

    var myURL = new URL(href)

    if (myURL.hostname === 'www.w3.org') {
      console.log('skipped: ' + myURL.toString())
      return myURL.toString()
    }

    /*
        Veeva Templates/Fragments
     */

    if (boilerplate === 'ae-fragment-flexi') {
      var addAppDocId = (myURL.hostname.toString() !== 'vv-agency-rmh.veevavault.com')
      if (addAppDocId) {
        myURL.searchParams.append('AppDocId', '123')
      }
      href = myURL.toString()
    }

    // For Adobe Campain
    if (boilerplate === 'ac-template-flexi') {
    }

    // For Salesforce Marketing Cloud
    if (boilerplate === 'asf-template-flexi') {
    }

    // Veeva Token
    href = href.replace(/%7B/g, '{')
    href = href.replace(/%7D/g, '}')
    href = href.replace(/%3A/g, ':')
    href = href.replace(/%3B/g, ';')

    // Adobe Token
    href = href.replace(/%3C%25%3D/g, '<%=')
    href = href.replace(/%25%3E/g, '%>')

    return href
  }

}
