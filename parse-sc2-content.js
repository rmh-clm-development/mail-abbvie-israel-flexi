module.exports = {

  parser: function (content, sc2Field, projectPath = false) {
    function removePtags (content) {
      content = content.replace(/<\/p><p>/g, '<br>')
      content = content.replace(/<\/?p>/g, '')
      content = content.replace(/<br><br><br>/g, '<br><br>')
      return content
    }

    // Example: include showcase data
    if (projectPath) {
      var _fields = require(projectPath + '/showcase/fields.json')
      const _services = require(projectPath + '/showcase/services.json')
      var boilerplate = _services.fragger.boilerplate
    }

    // free customRichText from paragraphs
    content = content.replace(/\{{customRichText}}/g, function (match, number) {
      return '</p>{{customRichText}}<p>'
    })
    // Remove emtpy paragraphs which might occor by replace above
    content = content.replace(/<p>\s*<\/p>/g, function (match, number) {
      return ''
    })

    // Remove HTML from cutomText Tokeb
    content = content.replace(/\{\{customText\[([^\]]+)\]\}\}/gm, function (fullMatch, options) {
      return '{{customText[' + options.replace(/<([^>]+)>/gm, '') + ']}}'
    })

    // Button Labels need to be without any <p>
    // element_cta_{{index}}_label
    // element_keymessage_{{index}}_button_{{button_index}}_label
    var regCtaLabel = /element_cta_[1-9]{1,2}_button_label/gi
    var regKeymessageLabel = /element_keymessage_[1-9]{1,2}_button_[1-9]{1}_label/gi
    var regTwoColLabel = /element_two_columns_[1-9_a-z]{1,20}_button_label/gi
    var regPreheader = /preheader/gi
    var isButtonLabel = sc2Field.match(regCtaLabel) || sc2Field.match(regKeymessageLabel) || sc2Field.match(regTwoColLabel) || sc2Field.match(regPreheader)
    if (isButtonLabel) {
      content = removePtags(content)
    }

    // Rating Element: Search for $_<varaible_$ and replace by SC property

    var regRatingUrl = /rating_[1-9]{1}_.*url/gi
    var isRatingUrl = sc2Field.match(regRatingUrl)

    if (isRatingUrl) {
      content = content.replace(/\$_([0-9a-z_]{1,50})_\$/ig, function (match, variable) {
        return _fields[variable]
      })
      return content
    }

    // In fragments > transform references and footnotes to Veeva Tokens

    if (boilerplate === 'ae-fragment-flexi') {
      if (sc2Field === 'references' || sc2Field === 'footnotes') {
        content = removePtags(content)
      }
      if (sc2Field === 'references') {
        /*
            content= [1] abc
            {{CitationStart}}{{CitationNumber[1]. abc{{CitationEnd}}
          */
        content = content.replace(/\[([0-9]{1,2})\]/g, function (match, number) {
          return '{{CitationNumber[' + number + ']}}.'
        })
        content = '{{CitationStart}}' + content + '<br>{{CitationEnd}}'
        return content
      }
      if (sc2Field === 'footnotes') {
        /*
            content= ]1[ abc
            {{FootnoteStart}}{{FootnoteSymbol[1]. abc{{FootnoteEND}}
          */

        content = content.replace(/\]([0-9]{1,2})\[/g, function (match, number) {
          return '{{FootnoteSymbol[' + number + ']}}'
        })
        content = '{{FootnoteStart}}' + content + '<br>{{FootnoteEnd}}'

        return content
      }

      // All other text fields in the Fragment

      // content= [1-3,4]
      content = content.replace(/\[([0-9-,]{1,20})\]/g, function (match, refs) {
        // refs= 1-3,4
        // For veeva-Fragments:
        // references = {{CitationNumber[1]}}-{{CitationNumber[3]}},{{CitationNumber[4]}}

        var references = refs.replace(/[0-9]{1,3}/g, function (singleRref) {
          // 1 -> {{CitationNumber[1]}}
          return '{{CitationNumber[' + singleRref + ']}}'
        })
        return references
      })

      // content= ]1[
      content = content.replace(/\]([0-9]{1,2})\[/g, function (match, number) {
        return '{{FootnoteSymbol[' + number + ']}}'
      })
    }
    // END if Fragment

    return content
  }

}
