module.exports = {

  adobe_campaign: {
    'recipient.salutation': 'Dr',
    'recipient.firstName': 'Peter',
    'recipient.lastName': 'Mueller'
  },

  veeva: {
    vaultUrl: 'https://vv-agency-rmh.veevavault.com/ui/#doc_info/',
    'accFname': 'Peter',
    'accTitle': 'Dr.',
    'accLname': 'Mueller',
    'userName': 'Johanna Schmitt',
    'User.Title': 'msc',
    'User.MobilePhone': '+49 151 555 12 123',
    'User.Phone': '+49 555 12 123',
    'userPhone': '+49 555 12 123',
    'userEmailAddress': 'j.schmitt@client-email.com',
    'userPhoto': '<img src="images/rep-portrait.png">',
    'Call2_vod__c.Cobrowse_URL_Participant_vod__c': 'https://engage.veeva.com/m.html?j=123456789&pwd=123456',

    /*
      As the showcase (--veeva=info) mode renders some HTML-styles around the values, URLs or values in HTML attributes would break.
      To prevent HTML stuff around token in the showcase-mode either use {{Account.Primary_Email_BI__c}}(nostyle) instead
      or add the field to the noStyle-array (see below).
      The (nostyle) option will be removed in the dist-task
     */

    'noStyle': ['Account.Id'],

    /*
      To send personalized mails, the veeva-vars can be overriden according to the mail recipient

      Example:
      grunt --project=dph-demo --variant=ET-0001-pradaxa-template-test test --mailto=john.doe@email.com
      will override the value {{accLname}} by 'Doe'
    */

    testVars: {
      'john.doe@email.com': {
        'accLname': 'Doe'
      }
    },

    simToken: {
      'InsertCitations': '1. This is the place where all fragment-citations will be displayed',
      'InsertFootnotes': '* This is the place where all fragment-footnotes will be displayed',
      'CitationStart': '<p class="refs-sim">',
      'CitationEnd': '</p>',
      'FootnoteStart': '<p class="refs-sim">',
      'FootnoteEnd': '</p>',
      'insertEmailFragments': '<tr><td style="padding-bottom: 15px"><div style="width: 100%; text-align: center; padding: 10px 0; border: 1px solid #aaaaaa; font-size: 15px; line-height: 25px; background-color: #f4f4f4">Fragments will be inserted here $2</div></td></tr>'
    }
  }

}
