module.exports = {
  process: function (content, projectPath) {
    //var _fields = require(projectPath + '/showcase/fields.json')
    const _services = require(projectPath + '/showcase/services.json')
    var boilerplate = _services.fragger.boilerplate
    // DocCheck (bMail) Specific CSS removal:
    if (boilerplate === 'bm-template-flexi') {
      // remove all body, a, img h1-h9 without class
      content = content.replace(/body\s{[^}]+}/gm, '')
      content = content.replace(/\n(img|(h[1-9]\s)?a(:[a-z]{1,20})?)\s\{[^}]+}/gm, '')
      // remove * [lang... wildcard styles which are used for the star ratings
      content = content.replace(/\*\s\[lang[^}]+}/gm, '')
    }
    /*
    if (boilerplate === 'ae-template-flexi') {
      content = content.replace(/<\/body>/gm, '$$$$trackimage$$$$</body>')
    }
     */
    return content
  }
}
